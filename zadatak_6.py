import abc
import re

#potrebno je napraviti klasu film sa atributom ime i aps. metodom ocijeni_film
class Film:
    def __init__(self,ime):
        self.ime=ime
    def ocijeni_film(self):
        pass

#sada je potrebno napraviti klasu Sci-Fi koja nasledjuje ovu klasu i ima dodatne atribute 
# specijalni efekti i vratolomije
class Sci_Fi(Film):
    def __init__(self,ime,specijalni_efekti,vratolomije):
        super().__init__(ime)
        self.specijalni_efekti = specijalni_efekti
        self.vratolomije = vratolomije
    
    def ocijeni_film(self):
        #ocjena se racuna kao prosjecna vrijednost specijalnih efekata i vratolomija
        return (self.specijalni_efekti + self.vratolomije)/2
    
#zatim klasa Dokumentarac isto nasledjuje klasu Film sa atributima vjerodostojnost i gledanost

class Dokumentarac(Film):
    def __init__(self,ime,vjerodostojnost,gledanost):
        super().__init__(ime)
        self.vjerodostojnost=vjerodostojnost
        self.gledanost=gledanost

    def ocijeni_film(self):
        #ovdje se ocjena racuna mnozenjem gledanosti sa vjerodostojnosti
        #racunacemo da se vjerodostojnost unosi procentualno tj 1-100%
        return self.gledanost*self.vjerodostojnost/100

#sada cemo provjeriti ocjene nekog filma

film1=Sci_Fi("The Martian",10,5)
film2=Dokumentarac("The Act of Killing",87,6)

#print(f"Ocjena Sci-Fi filma {film1.ime} je {film1.ocijeni_film()}")
#print(f"Ocjena Sci-Fi filma {film2.ime} je {film2.ocijeni_film()}")

#potrebno je napraviti listu filmova a zatim pomocu regexa napraviti listu filmova koji 
# sadrze THE ali ne i AND

def filtriraj(filmovilista):
    for clan in filmovilista:
        if re.search(".* the .*", clan.ime) or re.search("The.*",clan.ime):
            if not re.search(".* and .*", clan.ime):
                print(clan.ime)
    

filmovi=[
    Film("Scream"),
    Film("The King's Man"),
    Film("A hero"),
    Film("Fast and Furious"),
    Film("The Matrix"),
    Film("Shang-Chi and the Legend of The Ten Rings")]

filtriraj(filmovi)











