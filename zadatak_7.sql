CREATE TABLE FIRMA_ODELJENJA(
id_od int PRIMARY KEY,
naziv varchar(50),
lokacija varchar(50) DEFAULT "Podgorica" CHECK (lokacija IN ("Podgorica", "Niksic", "Zabljak", "Bar")),
rukovodilac varchar(50)
);

CREATE TABLE FIRMA_ZAPOSLENI(
id int(3) PRIMARY KEY,
od int,
ime varchar(40),
telefon varchar(12),
datum_zaposlenja date,
FOREIGN KEY(od) REFERENCES FIRMA_ODELJENJA(id_od)
);

INSERT INTO FIRMA_ODELJENJA VALUES (10,"Sektor 1", "Zabljak","Glavni rukovodilac");
INSERT INTO FIRMA_ODELJENJA VALUES (11,"Sektor 2", "Niksic","Pomocni rukovodilac");
INSERT INTO FIRMA_ODELJENJA VALUES (12,"Sektor 3", "Bar", "Pomocni rukovodilac");
INSERT INTO FIRMA_ODELJENJA VALUES (13,"Marketing","Podgorica","Glavni rukovodilac");


INSERT INTO FIRMA_ZAPOSLENI VALUES (1, 10, "Nikola Nikolic", "067-111-123","2003-07-21");
INSERT INTO FIRMA_ZAPOSLENI VALUES (2, 10, "Jovan Jovanovic", "067-333-123","2010-05-24");
INSERT INTO FIRMA_ZAPOSLENI VALUES (3, 11, "Kaca Pejovic", "067-333-444","2011-04-14");
INSERT INTO FIRMA_ZAPOSLENI VALUES (4, 12, "Goran Goranovic", "068-777-444","2007-10-11");
INSERT INTO FIRMA_ZAPOSLENI VALUES (5, 10, "Marko Markovic", "069-999-444","1999-11-11");
INSERT INTO FIRMA_ZAPOSLENI VALUES (6, 13, "Andrej Vukalovic", "067-676-144","2008-03-03");
INSERT INTO FIRMA_ZAPOSLENI VALUES (7, 13, "Anja Vukalovic", "067-123-456","2015-09-18");

SELECT * FROM FIRMA_ODELJENJA;
SELECT * FROM FIRMA_ZAPOSLENI;

SELECT ime FROM FIRMA_ZAPOSLENI;
SELECT zap.telefon FROM FIRMA_ZAPOSLENI zap, FIRMA_ODELJENJA ode WHERE zap.od=ode.id_od AND ode.lokacija="Niksic";

SELECT rukovodilac FROM FIRMA_ZAPOSLENI
INNER JOIN FIRMA_ODELJENJA ON od=id_od WHERE datum_zaposlenja=(SELECT min(datum_zaposlenja)FROM FIRMA_ZAPOSLENI) AND ime="Marko Markovic";

DELETE FROM FIRMA_ZAPOSLENI WHERE od = (SELECT id_od FROM FIRMA_ODELJENJA WHERE naziv="Marketing");
