from multiprocessing import connection
import pymysql

#prvo je potrebno da se povezemo na vec postojecu bazu zadatak_7
connection = pymysql.connect(host="localhost",
  user="root",
  password="8520",
  database="zadatak_7")
cursor = connection.cursor()

#s obzirom da smo se povezali sa bazom koja vec ima date podatke i tabele, potrebno je napraviti
#uput koji ce u slucaju da tabela koju pravimo vec postoji, obrisati postojecu da ne bi imali duple
delete_table_zaposleni = "drop table if exists FIRMA_ZAPOSLENI;"
delete_table_odeljenje = "drop table if exists FIRMA_ODELJENJA;"

create_table1=""" CREATE TABLE FIRMA_ODELJENJA(
id_od int PRIMARY KEY,
naziv varchar(50),
lokacija varchar(50) DEFAULT "Podgorica" CHECK (lokacija IN ("Podgorica", "Niksic", "Zabljak", "Bar")),
rukovodilac varchar(50)
);"""

create_table2=""" CREATE TABLE FIRMA_ZAPOSLENI(
id int(3) PRIMARY KEY,
od int,
ime varchar(40),
telefon varchar(12),
datum_zaposlenja date,
FOREIGN KEY(od) REFERENCES FIRMA_ODELJENJA(id_od)
);"""

odeljenja=['INSERT INTO FIRMA_ODELJENJA VALUES (10,"Sektor 1", "Zabljak","Glavni rukovodilac");',
'INSERT INTO FIRMA_ODELJENJA VALUES (11,"Sektor 2", "Niksic","Pomocni rukovodilac");',
'INSERT INTO FIRMA_ODELJENJA VALUES (12,"Sektor 3", "Bar", "Pomocni rukovodilac");',
'INSERT INTO FIRMA_ODELJENJA VALUES (13,"Marketing","Podgorica","Glavni rukovodilac");']

zaposleni=['INSERT INTO FIRMA_ZAPOSLENI VALUES (1, 10, "Nikola Nikolic", "067-111-123","2003-07-21");',
'INSERT INTO FIRMA_ZAPOSLENI VALUES (2, 10, "Jovan Jovanovic", "067-333-123","2010-05-24");',
'INSERT INTO FIRMA_ZAPOSLENI VALUES (3, 11, "Kaca Pejovic", "067-333-444","2011-04-14");',
'INSERT INTO FIRMA_ZAPOSLENI VALUES (4, 12, "Goran Goranovic", "068-777-444","2007-10-11");',
'INSERT INTO FIRMA_ZAPOSLENI VALUES (5, 10, "Marko Markovic", "069-999-444","1999-11-11");',
'INSERT INTO FIRMA_ZAPOSLENI VALUES (6, 13, "Andrej Vukalovic", "067-676-144","2008-03-03");',
'INSERT INTO FIRMA_ZAPOSLENI VALUES (7, 13, "Anja Vukalovic", "067-123-456","2015-09-18");']

name='SELECT ime FROM FIRMA_ZAPOSLENI;'
tell='SELECT zap.telefon FROM FIRMA_ZAPOSLENI zap, FIRMA_ODELJENJA ode WHERE zap.od=ode.id_od AND ode.lokacija="Niksic";'
najstariji='SELECT rukovodilac FROM FIRMA_ZAPOSLENI INNER JOIN FIRMA_ODELJENJA ON od=id_od WHERE datum_zaposlenja=(SELECT min(datum_zaposlenja)FROM FIRMA_ZAPOSLENI) AND ime="Marko Markovic";'
delete_users='DELETE FROM FIRMA_ZAPOSLENI WHERE od = (SELECT id_od FROM FIRMA_ODELJENJA WHERE naziv="Marketing");'

try:
     cursor.execute(delete_table_zaposleni)
     print("Tabela FIRMA_ZAPOSLENI je izbrisana")
     cursor.execute(delete_table_odeljenje)
     print("Tabela FIRMA_ODELJENJA je izbrisana")

     cursor.execute(create_table1)
     print("Nova FIRMA_ODELJENJA je napravljena")
     cursor.execute(create_table2)
     print("Napravljena je nova tabela FIRMA_ZAPOSLENI")


     for query in odeljenja:
       cursor.execute(query)
     print("Dodata odeljenja u FIRMA_ODELJENJA")
     
     for query in zaposleni:
       cursor.execute(query)
     print("Dodati zaposleni u FIRMA_ZAPOSLENI")

     connection.commit()

     #sada treba da izlistamo imena svih zaposlenih
     cursor.execute(name)
     rows = cursor.fetchall()
     print("Imena zaposlenih:")
     for row in rows:
       print(row[0])
       
      #sada  trebamo istampati telefonski broj ljudi kojima je lokacija Niksic
     cursor.execute(tell)
     rows=cursor.fetchall()
     print("Telefonski brojevi ljudi sa lokacijom Niksic:")
     for row in rows:
       print(row[0])

except Exception as e:
  print("Error: ",e)

try:
  #potrebno je istampati najstarijeg rukovodioca kojem je ime Marko Markovic
    cursor.execute(najstariji)
    row=cursor.fetchone()
    print(row)
  #potrebno je obrisati zaposlene koji rade u Marketingu 
    cursor.execute(delete_users)
    connection.commit()
    print("Radnici koji rade u Marketingu su obrisani")
except Exception as e:
  print("Error: ", e)



connection.close()