#dati dictionary
from readline import append_history_file


developers = { 
    "Marshal": ["Company website", "Animal recognition", "Fashion store website"], 
    "Ted": ["Plants watering system", "WHAT", "Animal recognition", "Food recognition"], 
    "Monica": ["NEXT website", "Fashion store website"], 
    "Phoebe": ["WHAT", "Company website", "NEXT website"] }

#dictionary projects gdje je key ime projekta,a value lista developera koji su radili na istom
#prvo cemo napraviti listu projekata

project_list = list(developers.values())
project_list_flat = [item for sublist in project_list for item in sublist] #stavljanje svih podlista u jednu listu
#Sada cemo od ove liste napraviti set da se projekti ne bi ponavljali

project_set = set(project_list_flat) #napravljen set
#sada kada imamo sve projekte mozemo napraviti novi dictionary
projects = {}
for project in project_set:
    projects[project]=[]
    for developer in developers:
        if project in developers[developer]:
            projects[project].append(developer)

#izmijenite ime svakog projekta u listi tako da mu naziv izgleda kao BIXBIT_imeprojekta
new_list=list(map(lambda orig_project : "BIXBIT_"+ orig_project,project_list_flat))
#print(new_list)

#napravite funkciju koja kada joj se proslijedi ime developera i lista projekata dodaje 
# ga u dictionary developers i vraća novi ukupan broj developera firme

def addDeveloper(name,projectList):
    developers[name]=projectList
    return len(developers)

#newlen=addDeveloper("Anja",["NEXT website", "Fashion store website"])
#print(developers)
#print(newlen)

#napravite funkciju koja kad joj se proslijedi ukupno vrijeme provedeno na projektu i zarada, dictionary 
#developers mijenja po principu:  “Marshal”: [(“Company website”, “240h”, 5000), (“Animal recognition”, “670h”, 20000)..]. 
# Primijetite da je value lista tuple-ova
def timeAndSalary(project,time,salary):
    newValue=(project,time,salary)
    for developer in developers:
        if project in developers[developer]:
            index=developers[developer].index(project)  #pomocu indexa nadjemo gdje se nalazi trazeni projekat
            developers[developer][index]=newValue



timeAndSalary("Company website", "240h", 5000)
timeAndSalary("Animal recognition", "670h", 20000)
timeAndSalary("Fashion store website", "300h", 9300)
timeAndSalary("Plants watering system", "800h", 23000)
timeAndSalary("WHAT", "430h", 11000)
timeAndSalary("Food recognition", "200h", 4100)
timeAndSalary("NEXT website", "500h", 17000)

#print(developers)

#napravite funkciju koja štampa svakog developera firme i za svakog koliko zaradi za 
#sat vremena po principu ukupna_zarada/ukupan_broj_sati. 
#Pazite da ukoliko je više developera radilo na projektu posmatrate kao da su jednako radili. 
#Dakle ukoliko ih je dvoje, zarada se dijeli na pola, ali i vrijeme. Return treba da 
# bude formata “developer X zaradjuje Y eura po satu”. 

#ako se  i zarada i vrijeme dijeli na pola u slucaju da je vise ljudi radilo na prijektu,
#odnos zarada/vrijeme ostaje isti,samim tim mozemo zanemariti to 

def hourlyRate():
    for developer in developers:
        salary=0
        hours=0
        for project in developers[developer]:
            salary += project[2]
            #za vrijeme imamo problem jer moramo prvo maci h a satim string pretvoriti u broj
            hours += int(project[1][0:len(project)-1])
        hourlyProfit = salary/hours
        print(f'developer {developer} earns {hourlyProfit} per hr')


hourlyRate()



        
            
        


            




    


