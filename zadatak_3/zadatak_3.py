import matplotlib.pyplot as plt
from zadatak_3_funkcija import csv_reader

x,y = csv_reader("february-covid-data.csv")
#posto nam treba dnevni prikaz potrebna nam je i lista dana
days=list(range(1,29)) #dani za mjesec februar
fig, (ax1, ax2) = plt.subplots(2)
fig.suptitle('February Covid Data')

ax1.plot(days,x)
ax2.plot(days, y)

plt.show()

