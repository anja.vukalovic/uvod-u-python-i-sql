import re
import datetime

class Osoba:
    def __init__(self,ime,prezime,jmbg,opstina,datum_rodjenja):
        self.ime=ime
        self.prezime=prezime
        self._jmbg=jmbg     #jmbg je protected
        self.opstina=opstina
        self.datum_rodjenja=datum_rodjenja
    
    #potrebno je napraviti gettere
    def get_ime(self):
        return self.ime
    
    def get_prezime(self):
        return self.prezime
    
    def get_jmbg(self):
        return self._jmbg
    
    def get_opstina(self):
        return self.opstina
    
    def get_datum_rodjenja(self):
        return self.datum_rodjenja
    
    #poretbno je napraviti settere
    def set_ime(self,a):
        self.ime=a

    def set_prezime(self,a):
        self.prezime=a

    def set_jmbg(self,a):
        #potrebno je napraviti validaciju jmbg broja
        #ovo je najlakse uraditi preko regular expressions-jmbg mora da bude 13 cifara i ni manje ni vise
        if re.search("^\d{13}$", a):
            self.jmbg=a
        else:
            raise TypeError("JMBG mora imati 13 cifara")
    
    def set_opstina(self,a):
        self.opstina=a

    def set_datum_rodjenja(self,a):
        #potrebno je napraviti validaciju da li je datum rodjenja upisan u datetime formatu
        if re.search("\d{4}-\d{2}-\d{2}",a):
            #takodje je potrebno provjeriti da li je datum kasniji od danasnjeg datuma
            datum= datetime.datetime.strptime(a,'%Y-%m-%d').date()
            trenutno=datetime.datetime.now().date()
            if trenutno > datum:
                self.datum_rodjenja=a
            else:
                raise TypeError("Datum rodjenja mora biti prije danasnjeg datuma")
        else:
            raise TypeError("Datum rodjenja mora biti unijet u formatu yyyy-mm-dd")
    #potrebno je preklopiti lt operator
    def __lt__(self, other):
        return self.datum_rodjenja < other.datum_rodjenja
    


#potrebno je napraviti funkciju koja vraca najstarijeg iz prosledjene liste
def najstariji_clan(lista):
    najstariji=lista[0]
    for clan in lista:
        if clan < najstariji:
            najstariji=clan
    return najstariji





ljudi=[Osoba("Anja", "Vukalovic", "0123456789123", "Niksic", "1998-07-21"),
Osoba("Jovana", "Jovanovic", "0123456789123", "Podgorica", "1993-04-21"),
Osoba("Nikola", "Nikolic", "0123456789123", "Berane", "1990-10-11"),
Osoba("Sava", "Savic", "666666689123", "Podgorica", "1999-03-05"),
Osoba("Luka", "Lukic", "3333222556677", "Danilovgrad", "1991-08-16")]

#ljudi[1].set_datum_rodjenja("2028-02403")
#ljudi[0].set_jmbg("1111111111111111111")

#j=najstariji_clan(ljudi)
#print(f"Najstariji clan liste je {j.ime}")