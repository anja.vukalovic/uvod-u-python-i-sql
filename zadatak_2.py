import json
import csv
from datetime import datetime, timedelta

with open('covid-data.json') as f:
    data = json.load(f)

#potrebno je izvuci podatke za februar 2021 - koliko je svakog dana bilo pozitivnih i hospitalizovanih
#to znaci da su nam od interesa datumi 202102...
#print(type(data)) mi trenutno imamo listu objekata-datuma
#ideja je napraviti novu listu koja sadrzi samo datume iz februara 2021

february=[]
for dates in data:
    if dates["date"]//100 == 202102:
        february.append(dates)

csv_file = open("february-covid-data.csv", "w", newline="")
writer = csv.writer(csv_file)
writer.writerow(("Positive", "Hospitalized"))

for february_info in february:
    our_row= (february_info['positiveIncrease'], february_info['hospitalizedIncrease'])
    writer.writerow(our_row)

csv_file.close()

#Potrebno je napraviti funkciju koja racuna procenat hospitalizacije na dnevnom nivou

def hospitalized_percentage(date):
    #pretpostavicemo da kada se poziva funckija datum se unosi u datetime formatu kao string
    #potrebno je maci crtice iz datuma
    newdate=int(date.replace("-",""))
    for dates in data:
        if dates["date"] == newdate:
            res=round(dates['hospitalized']/dates['positive']*100,2)  #zaokruzicemo procenat na 2 decimale
            print(f"On this date hospitalized percentage was {res} %")
        





hospitalized_percentage("2021-03-07")



