from cgi import parse_multipart
from zadatak_4 import Osoba

class Radnik(Osoba):
    def __init__(self,ime,prezime,jmbg,opstina,datum_rodjenja,zanimanje,plata,godine_staza):
        super().__init__(ime,prezime,jmbg,opstina,datum_rodjenja)
        self.zanimanje=zanimanje
        self.__plata=plata        #atribut plata je private
        self.godine_staza=godine_staza
    
    def godisnji_prihod(self):
        godpri=self.__plata*12
        god_rodjenja=self.datum_rodjenja[0:4]
        tp=(self.ime,self.prezime,god_rodjenja,self.godine_staza,godpri)
        return tp
    


#potrebno je napraviti funkcije koje sortiraju po godisnjim prihodima i godinama staza i godistu
#pokusacemo da napravimo funkciju koja moze da sortira po cemu god mi hocemo

def sortiraj(lista,po_cemu):
    nova_lista = []
    if po_cemu == "godisnji_prihodi":
        nova_lista= sorted(lista,key = lambda clan: clan.godisnji_prihod()[4])
    elif po_cemu == "godine_staza":
        nova_lista= sorted(lista,key = lambda clan: clan.godisnji_prihod()[3])
    elif po_cemu == "godiste":
        nova_lista= sorted(lista,key = lambda clan: clan.godisnji_prihod()[2])
    else:
        print("Ne mozemo sortirati listu za odredjeni kriterijum")


    return nova_lista

#sada je potrebno da napravimo radnike da bi testirali i sortirali liste
radnici=[Radnik("Anja", "Vukalovic", "0123456789123", "Niksic", "1998-07-21","FullStackDev",5000,3),
Radnik("Jovana", "Jovanovic", "0123456789123", "Podgorica", "1993-04-21","AndroidDev",1750,2),
Radnik("Nikola", "Nikolic", "0123456789123", "Berane", "1990-10-11","BackendDev",2500,6),
Radnik("Sava", "Savic", "666666689123", "Podgorica", "1999-03-05","FrontendDev",1300,1),
Radnik("Luka", "Lukic", "3333222556677", "Danilovgrad", "1991-08-16","iOSDev",4800,5)]

po_godisnjim_prihodima=sortiraj(radnici,"godisnji_prihodi")
po_godinama_staza=sortiraj(radnici,"godine_staza")
po_godistu=sortiraj(radnici,"godiste")
#test_greska=sortiraj(radnici,"nestodrugo")

if po_godinama_staza == po_godistu:
    print("Najstariji radnici imaju najduzi radni staz")
else:
    print("Liste nisu iste sto znaci da najstariji radnici nemaju i najduzi radni staz")