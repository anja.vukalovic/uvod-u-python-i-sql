import csv

#potrebno je napraviti funkciju koja ce da cita podatke iz naseg csv fajla

def csv_reader(csv_file):
    f=open(csv_file,'r')  #potrebno nam je samo citanje fajla
    csvreader=csv.reader(f)
    header=next(csvreader)  #preskacemo prvi dio tj header fajla jer nam tu nisu info vec naziv kolone
    #posto cemo pozivati ovu funkciju radi grafickog prikaza, napravicemo dvije liste u koje
    #cemo upisati prvu i drugu kolonu tj hospitalizovane i pozitivne slucajeve u februaru
    x=[]
    y=[]
    for row in f:
        #posto izmedju svakog broja imamo zarez, potrebno je odvojiti vrijednosti desno i lijevo od zareza
        info=row.split(',')  #funkcija radi sa stringovima pa je potrebno vratiti vrijednost na broj
        x.append(int(info[0]))
        y.append(int(info[1]))

    return x,y

#x,y = csv_reader("february-covid-data.csv")
#print(y)